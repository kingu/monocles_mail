package de.monocles.mail.mail;

public enum ConnectionSecurity {
    NONE,
    STARTTLS_REQUIRED,
    SSL_TLS_REQUIRED
}
