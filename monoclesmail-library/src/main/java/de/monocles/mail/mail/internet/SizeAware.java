package de.monocles.mail.mail.internet;


public interface SizeAware {
    long getSize();
}
