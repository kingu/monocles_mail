package de.monocles.mail.mail.store.imap;


import java.io.IOException;
import java.util.List;

import de.monocles.mail.mail.MessagingException;


interface ImapSearcher {
    List<ImapResponse> search() throws IOException, MessagingException;
}
