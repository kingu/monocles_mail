
package de.monocles.mail.mail.transport;

import de.monocles.mail.mail.monoclesmailLib;
import de.monocles.mail.mail.Message;
import de.monocles.mail.mail.MessagingException;
import de.monocles.mail.mail.Transport;
import de.monocles.mail.mail.store.StoreConfig;
import de.monocles.mail.mail.store.webdav.WebDavHttpClient;
import de.monocles.mail.mail.store.webdav.WebDavStore;
import timber.log.Timber;

import java.util.Collections;

public class WebDavTransport extends Transport {
    private WebDavStore store;

    public WebDavTransport(StoreConfig storeConfig) throws MessagingException {
        store = new WebDavStore(storeConfig, new WebDavHttpClient.WebDavHttpClientFactory());

        if (monoclesmailLib.isDebug())
            Timber.d(">>> New WebDavTransport creation complete");
    }

    @Override
    public void open() throws MessagingException {
        if (monoclesmailLib.isDebug())
            Timber.d( ">>> open called on WebDavTransport ");

        store.getHttpClient();
    }

    @Override
    public void close() {
    }

    @Override
    public void sendMessage(Message message) throws MessagingException {
        store.sendMessages(Collections.singletonList(message));
    }
}
