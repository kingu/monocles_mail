# monocles mail

monocles mail is an open-source e-mail client for Android based on the old K-9 Mail layout (Version 5.600).


## Download

monocles mail can be downloaded from a couple of sources:

- [F-Droid](https://f-droid.org/repository/browse/?fdid=de.monocles.mail)
- [Google Play](https://play.google.com/store/apps/details?id=de.monocles.mail)
- [Codeberg Releases](https://codeberg.org/Arne/monocles_mail/releases)

You might also be interested in becoming a [tester](https://codeberg.org/Arne/monocles_mail) to try new versions early.


## Release Notes

Read the [Release Notes](https://codeberg.org/Arne/monocles_mail/releases) and learn what changed in each version.


## Need Help?

If the app is not behaving like it should, you might find these resources helpful:

- [User Manual](https://monoclesmail.github.io/documentation.html)
- [Frequently Asked Questions](https://monoclesmail.github.io/documentation/faq.html)


## Translations

Help translate monocles mail by forking the repository.


## Design

The design is based on the old K-9 mail layout many asked for.


## Contributing

Please fork this repository and contribute back using [pull requests](https://codeberg.org/Arne/monocles_mail/pulls).

Any contributions, large or small, major features, bug fixes, unit/integration tests are welcomed and appreciated,
and will be thoroughly reviewed and discussed.
Please make sure you read the [Code Style Guidelines](https://codeberg.org/Arne/monocles_mail/wiki/CodeStyle).


## Communication

Aside from discussing changes in [pull requests](https://codeberg.org/Arne/monocles_mail/pulls) and
[issues](https://codeberg.org/Arne/monocles_mail/issues) we use the following communication services:

- XMPP chat, [monocles support](xmpp:support@conference.monocles.de)
- XMPP chat, [monocles development](xmpp:development@conference.monocles.de)
- [E-Mail](mailto:support@monocles.de)


## License

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    
## Donation

Would you like to support the project with a donation: https://monocles.de/more/#donation-section
