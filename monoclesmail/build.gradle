apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'

if (!rootProject.optimizeForDevelopment) {
    apply from: '../gradle/plugins/checkstyle-android.gradle'
    apply from: '../gradle/plugins/findbugs-android.gradle'
}

if (rootProject.testCoverage) {
    apply plugin: 'jacoco'
}

//noinspection GroovyAssignabilityCheck
configurations.all {
    resolutionStrategy {
        force "com.android.support:support-annotations:${androidSupportLibraryVersion}"
    }
}

dependencies {
    implementation project(':monoclesmail-library')
    implementation project(':plugins:HoloColorPicker')
    implementation project(':plugins:openpgp-api-lib:openpgp-api')
    implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${kotlinVersion}"
    implementation "com.squareup.okio:okio:${okioVersion}"
    implementation 'commons-io:commons-io:2.4'
    implementation "com.android.support:support-v4:28.0.0"
    implementation 'org.jsoup:jsoup:1.11.2'
    implementation 'de.cketti.library.changelog:ckchangelog:1.2.1'
    implementation 'com.github.bumptech.glide:glide:3.6.1'
    implementation 'com.splitwise:tokenautocomplete:2.0.7'
    implementation 'de.cketti.safecontentresolver:safe-content-resolver-v14:0.9.0'
    implementation 'com.github.amlcurran.showcaseview:library:5.4.1'
    implementation 'com.squareup.moshi:moshi:1.2.0'
    implementation "com.jakewharton.timber:timber:${timberVersion}"
    implementation 'net.jcip:jcip-annotations:1.0'
    implementation 'org.apache.james:apache-mime4j-core:0.8.1'

    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.2'

    testImplementation "org.robolectric:robolectric:${robolectricVersion}"
    testImplementation "junit:junit:${junitVersion}"
    testImplementation "com.google.truth:truth:${truthVersion}"
    testImplementation "org.mockito:mockito-core:${mockitoVersion}"
    testImplementation "org.jdom:jdom2:2.0.6"
}

android {
    compileSdkVersion androidCompileSdkVersion.toInteger()
    buildToolsVersion androidBuildToolsVersion

    defaultConfig {
        applicationId "de.monocles.mail"
        testApplicationId "de.monocles.mail.tests"

        versionCode 1
        versionName '1.0'

        minSdkVersion 15
        targetSdkVersion 29

        generatedDensities = ['mdpi', 'hdpi', 'xhdpi']

        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
    }

    signingConfigs {
        release
    }

    buildTypes {
        release {
            if (project.hasProperty('storeFile')) {
                signingConfig signingConfigs.release
            }

            minifyEnabled true
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'

            buildConfigField "boolean", "DEVELOPER_MODE", "false"
        }

        debug {
            applicationIdSuffix ".debug"
            testCoverageEnabled rootProject.testCoverage

            buildConfigField "boolean", "DEVELOPER_MODE", "true"
        }
    }

    // Do not abort build if lint finds errors
    lintOptions {
        abortOnError false
        lintConfig file("$rootProject.projectDir/config/lint/lint.xml")
    }

    packagingOptions {
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/NOTICE.txt'
        exclude 'META-INF/README'
        exclude 'LICENSE.txt'
    }

    compileOptions {
        sourceCompatibility JavaVersion.VERSION_1_8
        targetCompatibility JavaVersion.VERSION_1_8
    }
}

if (project.hasProperty('keyAlias')) {
    android.signingConfigs.release.keyAlias = keyAlias
}
if (project.hasProperty('keyPassword')) {
    android.signingConfigs.release.keyPassword = keyPassword
}
if (project.hasProperty('storeFile')) {
    android.signingConfigs.release.storeFile = file(storeFile)
}
if (project.hasProperty('storePassword')) {
    android.signingConfigs.release.storePassword = storePassword
}
