package de.monocles.mail.ui.messageview;


public interface OnCryptoClickListener {
    void onCryptoClick();
}
