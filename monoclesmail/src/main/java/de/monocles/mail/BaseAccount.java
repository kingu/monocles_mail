package de.monocles.mail;

public interface BaseAccount {
    String getEmail();
    void setEmail(String email);
    String getDescription();
    void setDescription(String description);
    String getUuid();
}
