package de.monocles.mail.message.html

data class UriMatch(
        val startIndex: Int,
        val endIndex: Int,
        val uri: CharSequence
)
