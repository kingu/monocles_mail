package de.monocles.mail.message;


import java.util.List;

import de.monocles.mail.crypto.MessageCryptoStructureDetector;
import de.monocles.mail.mail.Message;
import de.monocles.mail.mail.Part;


public class ComposePgpEnableByDefaultDecider {
    public boolean shouldEncryptByDefault(Message localMessage) {
        return messageIsEncrypted(localMessage);
    }

    private boolean messageIsEncrypted(Message localMessage) {
        List<Part> encryptedParts = MessageCryptoStructureDetector.findMultipartEncryptedParts(localMessage);
        return !encryptedParts.isEmpty();
    }
}
