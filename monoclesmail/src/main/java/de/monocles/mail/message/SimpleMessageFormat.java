package de.monocles.mail.message;


public enum SimpleMessageFormat {
    TEXT,
    HTML
}
