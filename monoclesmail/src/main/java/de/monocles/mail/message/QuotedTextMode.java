package de.monocles.mail.message;


public enum QuotedTextMode {
    NONE,
    SHOW,
    HIDE
}
