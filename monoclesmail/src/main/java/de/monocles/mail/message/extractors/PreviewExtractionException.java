package de.monocles.mail.message.extractors;


class PreviewExtractionException extends Exception {
    public PreviewExtractionException(String detailMessage) {
        super(detailMessage);
    }
}
