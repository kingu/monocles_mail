package de.monocles.mail.mailstore.migrations;


import android.database.sqlite.SQLiteDatabase;


class MigrationTo61 {
    public static void removeErrorsFolder(SQLiteDatabase db) {
        db.execSQL("DELETE FROM folders WHERE name = 'monoclesmail-errors'");
    }
}
