package de.monocles.mail.mailstore.migrations;


import java.util.List;

import android.content.Context;

import de.monocles.mail.Account;
import de.monocles.mail.mail.Flag;
import de.monocles.mail.mailstore.LocalStore;
import de.monocles.mail.preferences.Storage;


/**
 * Helper to allow accessing classes and methods that aren't visible or accessible to the 'migrations' package
 */
public interface MigrationsHelper {
    LocalStore getLocalStore();
    Storage getStorage();
    Account getAccount();
    Context getContext();
    String serializeFlags(List<Flag> flags);
}
