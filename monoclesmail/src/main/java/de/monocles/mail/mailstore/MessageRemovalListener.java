package de.monocles.mail.mailstore;

import de.monocles.mail.mail.Message;

public interface MessageRemovalListener {
    public void messageRemoved(Message message);
}
