package de.monocles.mail.controller.imap;


import android.content.Context;

import de.monocles.mail.Account;
import de.monocles.mail.controller.MessagingController;
import de.monocles.mail.controller.MessagingListener;
import de.monocles.mail.controller.RemoteMessageStore;
import de.monocles.mail.mail.Folder;
import de.monocles.mail.notification.NotificationController;


public class ImapMessageStore implements RemoteMessageStore {
    private final ImapSync imapSync;


    public ImapMessageStore(NotificationController notificationController, MessagingController controller,
            Context context) {
        this.imapSync = new ImapSync(notificationController, controller, context);
    }

    @Override
    public void sync(Account account, String folder, MessagingListener listener, Folder providedRemoteFolder) {
        imapSync.sync(account, folder, listener, providedRemoteFolder);
    }
}
