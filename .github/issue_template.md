Please search to check for an existing issue (including closed issues, for which the fix may not have yet been released) before opening a new issue: https://codeberg.org/Arne/monocles_mail/issues?q=is%3Aissue

### Expected behavior
Tell us what should happen

### Actual behavior
Tell us what happens instead

### Steps to reproduce
1.
2.
3.

### Environment
monocles mail version:

Android version:

Account type (IMAP, POP3, WebDAV/Exchange):
