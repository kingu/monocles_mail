## Submitting issues

If the app is not behaving like it should, it's not necessarily a bug. Please consult the following resources before
submitting a new issue.

* [User Manual](https://codeberg.org/Arne/monocles_mail/wiki)
* [Frequently Asked Questions](https://codeberg.org/Arne/monocles_mail)
* [Support Forum/Mailing List](mailto:support@monocles.de)



## Translations

We're using [Transifex](https://www.transifex.com/monocles/monoclesmail/) to manage translations.


## Contributing code

We love [pull requests](https://codeberg.org/Arne/monocles_mail/pulls) from everyone!

Any contributions, large or small, major features, bug fixes, unit/integration tests are welcomed and appreciated
but will be thoroughly reviewed and discussed.
Please make sure you read the [Code Style Guidelines](https://codeberg.org/Arne/monocles_mail/wiki/CodeStyle).
