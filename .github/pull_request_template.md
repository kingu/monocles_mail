Please ensure that your pull request meets the following requirements - thanks !

* Follows our existing [codestyle](https://codeberg.org/Arne/monocles_mail/wiki/CodeStyle).
* Does not break any unit tests.
* Contains a reference to the issue that it fixes.
* For cosmetic changes add one or multiple images, if possible.


