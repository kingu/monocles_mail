monocles mail is a fork of the K-9 mail version 5.600 with the old K-9 mail layout.

It comes with all the known functions of K-9 mail and will get some updates in the UI to improve usability.

Features:
* supports multiple accounts
* Unified Inbox
* privacy-friendly (no tracking whatsoever, only connects to your email provider)
* automatic background synchronization or push notifications
* local and server-side search
* OpenPGP email encryption (PGP/MIME)
